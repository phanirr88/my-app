import React from 'react';
import './Workspace.css';
import { Input } from 'antd';

const { Search } = Input;
class Workspace extends React.Component{
    
    constructor(props){
        super(props);
        this.state={ }
    }

    render(){
        return(
            <div className="wsgroup">
                <h5>Workspace</h5>
                <Search
      placeholder="filter board"
      onSearch={value => console.log(value)}
      style={{ width: 175 }}
    />
            </div>
        );
    }

}
export{ Workspace }