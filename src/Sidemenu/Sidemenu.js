import React from 'react';
import './Sidemenucss.css';
import { IoIosNotificationsOutline } from "react-icons/io";
import { RiDownload2Line } from "react-icons/ri";
import { FiCalendar } from "react-icons/fi";
import { FiUserPlus } from "react-icons/fi";
import { FiSearch } from "react-icons/fi";
import { AiOutlineQuestion } from "react-icons/ai";
import {FaRegUserCircle} from "react-icons/fa";
import { AiOutlineMenuUnfold } from "react-icons/ai";

class Sidemenu extends React.Component{
    
        constructor(props){
            super(props);
            this.state={ }
        }

        render(){
            return(
                
                <div className="sidegroup">
                    <div className="menuicon"><AiOutlineMenuUnfold color="#ff5858c7" size={ 30}/></div>
                      <div  className="listgroup">
                         <ul>
                             <li><a><IoIosNotificationsOutline color="#fff" size={25}/></a></li>
                             <li><a><RiDownload2Line color="#fff"  size={25}/></a></li>
                         </ul>
                      </div>

                      <div className="bottomlistgroup">
                          <ul>
                              <li><FiCalendar color="#fff" size={25}/></li>
                              <li><FiUserPlus color="#fff" size={25}/></li>
                              <li><FiSearch color="#fff" size={25}/></li>
                              <li><AiOutlineQuestion color="#fff" size={25}/></li>
                              <li><FaRegUserCircle color="#fff" size={36}/></li>
                          </ul>
                      </div>
                            
                </div>

            );
        }

}
export{ Sidemenu};
