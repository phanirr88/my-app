import React from 'react';
import { Table } from 'antd';
import 'antd/dist/antd.css';
import './Drag.css';
import { sortableContainer, sortableElement, sortableHandle } from 'react-sortable-hoc';
import { MenuOutlined ,DownOutlined,StepBackwardOutlined  } from '@ant-design/icons';
import arrayMove from 'array-move';
import { Menu, Dropdown } from 'antd';
import {FaRegUserCircle} from "react-icons/fa";
//import {FaRegUserCircle} from "react-icons/fa";
//import { DownOutlined } from '@ant-design/icons';



const DragHandle = sortableHandle(() => (
  <MenuOutlined style={{ cursor: 'pointer', color: '#999' }} />
));

const columns = [
  {
    title: ' ',
    dataIndex: 'sort',
    width: 30,
    className: 'drag-visible',
    render: () => <DragHandle />,
  },
  {
    title: 'Things to do',
    dataIndex: 'name',
    className: 'drag-visible',
  },
  {
    title: 'Owner',
    dataIndex: 'age',
  },
  {
    title: 'Status',
    dataIndex: 'address',
  },
];

const data = [
  {
    key: '1',
    name: 'New Item1',
    age: <FaRegUserCircle size={25}/>,
    address: 'Working on it',
    index: 0,
  },
  {
    key: '2',
    name: 'New Item2',
    age: <FaRegUserCircle size={25}/>,
    address: 'Stuck',
    index: 1,
  },
  {
    key: '3',
    name: 'New Item3',
    age: <FaRegUserCircle size={25}/>,
    address: 'Waiting for Review',
    index: 2,
  },
  {
    key: '4',
    name: 'New Item4',
    age: <FaRegUserCircle size={25}/>,
    address: 'Done',
    index: 3,
  },
  {
    key: '5',
    name: 'New Item5',
    age: <FaRegUserCircle size={25}/>,
    address: 'Stuck',
    index: 4,
  },
  {
    key: '6',
    name: 'New Item6',
    age: <FaRegUserCircle size={25}/>,
    address: 'Done',
    index: 5,
  },
  {
    key: '7',
    name: 'New Item7',
    age: <FaRegUserCircle size={25}/>,
    address: 'Done',
    index: 6,
  },
];

const SortableItem = sortableElement(props => <tr {...props} />);
const SortableContainer = sortableContainer(props => <tbody {...props} />);
const DragableBodyRow = ({ index, className, style, ...restProps }) => (
  <SortableItem index={restProps['data-row-key']} {...restProps} />
);

class Drag extends React.Component {
  state = {
    dataSource: data,
  };

  onSortEnd = ({ oldIndex, newIndex }) => {
    const { dataSource } = this.state;
    if (oldIndex !== newIndex) {
      const newData = arrayMove([].concat(dataSource), oldIndex, newIndex).filter(el => !!el);
      console.log('Sorted items: ', newData);
      this.setState({ dataSource: newData });
    }
  };

  render() {
    const { dataSource } = this.state;
    const DraggableContainer = props => (
      <SortableContainer
        useDragHandle
        helperClass="row-dragging"
        onSortEnd={this.onSortEnd}
        {...props}
      />
    );
    const menu = (
      <Menu>
        <Menu.Item key="0">
          <a href="http://www.alipay.com/">1st menu item</a>
        </Menu.Item>
        <Menu.Item key="1">
          <a href="http://www.taobao.com/">2nd menu item</a>
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="3">3rd menu item</Menu.Item>
      </Menu>
    );
    return (
       <div className ="tabledashboard">
         <div className="dsheader"> 
             <h5>Web Design</h5>
             <Dropdown overlay={menu} trigger={['click']}>
                  <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                    Click me <DownOutlined />
                  </a>
               </Dropdown>
               
         </div>
        <Table
              pagination={false}
              dataSource={dataSource}
              columns={columns}
              rowKey="index"
              components={{
                body: {
                  wrapper: DraggableContainer,
                  row: DragableBodyRow,
                },
              }}
      />

       </div>
     
    );
  }
}
export {Drag } ;
// npm install react-sortable-hoc
// npm install array-move
// npm install @ant-design/icons
