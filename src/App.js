import React from 'react';
 
import './App.css';
import { Sidemenu } from './Sidemenu/Sidemenu';
import {Drag} from './Drag/Drag';
import { Workspace } from './Workspace/Worksapce';

function App() {
  return (
    <div className="screengroup">

      <div>
              <Sidemenu/>
              <Workspace/>
              <Drag/>
      </div>

    </div>


  );
}

export default App;
